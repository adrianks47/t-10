package RegistryModelPack;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FuelPrice {
    private float price;
    private int type;
    private String date;

    public void setPrice(float price) {
        this.price = price;
    }
    public void setType(int type) {
        this.type = type;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
    public int getType() {
        return type;
    }
    public float getPrice() {
        return price;
    }
    
    static String[] typeName = {"Gasolina","Etanol","Diesel"};
    private static DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    public String toStringFileMode()
    {
        String s;
        s  = Float.toString(price) + "\r\n";
        s += Integer.toString(type) + "\r\n";
        s += date + "\r\n";
        
        return s;
    }
    public FuelPrice(float price, int type ) {
        this.price = price;
        this.type = type;
        this.date = dateFormat.format( new Date() );
    }
    public FuelPrice(float price, int type,  String date ) {
        this.price = price;
        this.type = type;
        this.date = date;
    }
    
    @Override
    public String toString() {
        return date +" - "+typeName[type]+" = "+ price +" R$";
    }
}
