package MainPack;

import RegistryControlPack.RegistryControl;
import RegistryModelPack.RegistryModel;
import RegistryViewPack.RegistryView;
import PersistencePackage.RegistryPersistence;
public class MainGasStationRegistry {

    static public RegistryControl control;
    static public RegistryModel model;
    static public RegistryView view;
    static public Object[] emptyList = {};
    static public RegistryPersistence persistence;
    
    public static void main(String[] args) {
        persistence = new RegistryPersistence();
        model = new RegistryModel();
        view = new RegistryView();
        control = new RegistryControl();
    }
    
}
