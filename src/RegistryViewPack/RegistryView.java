package RegistryViewPack;

import java.util.ArrayList;
import MainPack.MainGasStationRegistry;
import RegistryModelPack.GasStation;

public class RegistryView {
    private final MainViewWindow window;
    private int imgW;
    private int imgH;

    public int getImgW() {
        return imgW;
    }
    public int getImgH() {
        return imgH;
    }
    
    public void UpdateStationInfoTable( GasStation station ){
        window.UpdateStationInfoTable(station);
    }
    public void UpdateStationList( Object[] list ){
        window.UpdateStationList(list);
    }
    public void UpdateStationPriceList( Object[] list ){
        window.UpdateStationPriceList(list);
    }
    public void SetEditTabValues( GasStation gs ) {
        window.SetEditTabValues( gs );
    }
    public void SetFilterLabel( String s ) {
        window.SetFilterLabel( s );
    }
    public RegistryView(){
        window = new MainViewWindow();
        imgW = window.getImgW();
        imgH = window.getImgH();
    }
}
