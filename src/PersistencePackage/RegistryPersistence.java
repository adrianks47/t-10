package PersistencePackage;

import MainPack.MainGasStationRegistry;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.FileReader;
import java.io.BufferedReader;
import RegistryModelPack.GasStation;
import RegistryModelPack.FuelPrice;

import java.util.ArrayList;

public class RegistryPersistence {
    
    private String fileName;
    
    public void SaveAllData( ){
        
        /////// FILE CHECK
        File f = new File(fileName);
        if( !f.exists() )
        {
            try{ f.createNewFile(); }
            catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        /////////// GET DATA
        String allData;
        allData = MainGasStationRegistry.model.GetDataString();
        
        /////// WRITER
        PrintWriter writer;
        try{ writer = new PrintWriter( fileName , "UTF-8"); }
        catch( FileNotFoundException e ){
            System.out.print("File not found");
            return;
        }
        catch (UnsupportedEncodingException e) {
            System.out.print("Unsupported encoding");
            return;
        }
        writer.print(allData);
        writer.close();
                
    }
    public void LoadAllData()
    {
        /////// FILE CHECK
        File f = new File(fileName);
        if( !f.exists() )
        {
            try{ f.createNewFile(); }
            catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        ////////// DESELECT
        MainGasStationRegistry.control.SetSelectedStation( -1 );
        MainGasStationRegistry.view.UpdateStationInfoTable(null);
        
        
        /////////// CLEAR DATA
        MainGasStationRegistry.model.ClearModel();
        
        ////// LOAD
        
        FileReader fileReader;
                
        try{
            fileReader = new FileReader( f );
        }
        catch( FileNotFoundException e ){
            System.out.print("File not found");
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> lines = new ArrayList();
        String line;
        
        try{ line = bufferedReader.readLine(); }
        catch( IOException e ){return;}
        
        while ( line != null) {

            lines.add(line);
            try{ line = bufferedReader.readLine(); }
            catch( IOException e ){return;}
        }
        /////////////////// PARSE
        
        int postoN = Integer.parseInt( lines.get( 0 ) );
        int offset = 1;
        int cont = 0;
        while( cont < postoN )
        {
            GasStation gs = new GasStation( lines.get( offset+0 ),lines.get( offset+1 ),lines.get( offset+2 ),lines.get( offset+3 ), lines.get( offset+4 ), lines.get( offset+5 ), lines.get( offset+6 ), lines.get( offset+7 )  );
            offset += 8;
            
            int fuelN =  Integer.parseInt( lines.get( offset ) );
            offset += 1;
            
            int cont2 = 0;
            while( cont2 < fuelN )
            {
                float price = Float.parseFloat( lines.get( offset+0 ) );
                int type = Integer.parseInt( lines.get( offset+1 ) );
                FuelPrice fp = new FuelPrice( price, type ,lines.get( offset+2 ) );
                gs.AddPrice(fp);
                
                offset += 3;
                cont2++;
            }
            MainGasStationRegistry.model.AddNewStation(gs);
            cont++;
        }
                
                        
        //// Update interface
        MainGasStationRegistry.view.UpdateStationList( MainGasStationRegistry.model.GetStationList() );
    }
    public RegistryPersistence( ){
        fileName = "savedData.txt";
    }
}
