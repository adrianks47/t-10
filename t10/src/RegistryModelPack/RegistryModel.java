package RegistryModelPack;

import MainPack.MainGasStationRegistry;
import java.util.ArrayList;
import RegistryModelPack.FuelPrice;
public class RegistryModel {
    private final ArrayList< GasStation > gasStations;
    private GasStation selectedStation = null;
    private final ArrayList< GasStation > gasStationsFiltrated;
    private boolean isFiltrated = false;
    public void SetCurrentStationValues( String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira )
    {
        if( selectedStation != null ){
            selectedStation.SetValues( CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira );
            selectedStation = null;
            MainGasStationRegistry.view.UpdateStationList( gasStations.toArray() );
            MainGasStationRegistry.view.UpdateStationInfoTable( selectedStation );
        }
        else
            System.out.print("NADA SELECIONADO \n");
    }
    public void FilterStation( String bairro )
    {
        gasStationsFiltrated.clear();
        int i = 0;
        while( i < gasStations.size() )
        {
            GasStation gs = gasStations.get(i);
            if( gs.getBairro().contains(bairro) )
                gasStationsFiltrated.add(gs);
            i++;
        }
        MainGasStationRegistry.view.UpdateStationList( gasStationsFiltrated.toArray() );
        MainGasStationRegistry.view.SetFilterLabel( "(y)" );
        isFiltrated = true;
    }
    public void SetSelectedStation( int i )
    {
        if( i < 0 )
            selectedStation = null;
        else if( isFiltrated )
            selectedStation = gasStationsFiltrated.get(i);
        else selectedStation = gasStations.get(i);
        MainGasStationRegistry.view.UpdateStationInfoTable( selectedStation );
        MainGasStationRegistry.view.SetEditTabValues( selectedStation );
    }
    public GasStation getStation( String razSocial )
    {
        int cont = 0;
        while( cont < gasStations.size() )
        {
            if( gasStations.get(cont).getRazaoSocial().equalsIgnoreCase(razSocial) )
                return gasStations.get(cont);
            cont++;
        }
        return null;
    }
    public void AddNewStation( GasStation gs ) {
        gasStations.add( gs );
    }
    ///////////////////// Adition and removal
    public void AddNewStation(String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira)
    {
        GasStation gs = new GasStation( CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira );
        gasStations.add( gs );
        gasStationsFiltrated.clear();
        isFiltrated = false;
        MainGasStationRegistry.view.SetFilterLabel( "(n)" );
        MainGasStationRegistry.view.UpdateStationList( gasStations.toArray() );
    }
    public void DeleteSelectedStation( ){
        
        gasStations.remove( selectedStation );
        selectedStation = null;
        gasStationsFiltrated.clear();
        isFiltrated = false;
        MainGasStationRegistry.view.SetFilterLabel( "(n)" );
        MainGasStationRegistry.view.UpdateStationList( gasStations.toArray() );
        MainGasStationRegistry.view.UpdateStationInfoTable( selectedStation );
    }
    public void RegisterFuelPrice( float price, int type ) {
        selectedStation.AddPrice( price, type );
        MainGasStationRegistry.view.UpdateStationPriceList( selectedStation.GetPriceArray() );
    }
    
    public Object[] GetStationList() {
        return gasStations.toArray();
    }
    public Object[] GetFuelPriceList() {
        return selectedStation.GetPriceArray();
    }
    ///////////////////////////////////// construct
    public RegistryModel(){
        gasStations = new ArrayList();
        gasStationsFiltrated = new ArrayList();
    }
    
    public FuelPrice GetCurStationLastPriceOfType( int type )
    {
        return selectedStation.GetCurStationLastPriceOfType( type );
    }
    public void ClearModel()
    {
        int i = 0;
        while( i < gasStations.size() ) {
            gasStations.get(i).Clear();
            i++;
        }
        gasStations.clear();
    }
    public String GetDataString()
    {
        //// model = [N de postos][ Dados do posto [N de preços][preços][preços][...] ][]
        String s;
        s = Integer.toString( gasStations.size()  );
        s += "\r\n";
        int i = 0;
        while( i < gasStations.size() )
        {
            s += gasStations.get(i).toStringAllFields();
            i++;
        }
        return s;
    }
}
