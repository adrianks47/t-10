package RegistryModelPack;

import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import javax.swing.ImageIcon;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import MainPack.MainGasStationRegistry;
public class GasStation {
    private String CNPJ;
    private String razaoSocial;
    private String nomeFantasia;
    private String endereco;
    private String bairro;
    private String CEP;
    private String imagem;
    private String bandeira;
    
    private BufferedImage icon ;
    private ImageIcon realIcon;
    
    private ArrayList<FuelPrice> prices;
    
    public void Clear(){
        prices.clear();
        if(icon != null)
            icon.flush();
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }
    public ImageIcon GetReadIcon(){
        return realIcon;
    }
    public String toStringAllFields()
    {
        String s;
        s = CNPJ + "\r\n";
        s += razaoSocial + "\r\n";
        s += nomeFantasia + "\r\n";
        s += endereco + "\r\n";
        s += bairro + "\r\n";
        s += CEP + "\r\n";
        s += imagem + "\r\n";
        s += bandeira + "\r\n";
        
        s += Integer.toString( prices.size()  ) + "\r\n";
        int i = 0;
        while( i < prices.size() )
        {
            s += prices.get(i).toStringFileMode();
            i++;
        }
        return s;
    }
    public void AddPrice( FuelPrice fp ) {
        prices.add(fp);
    }
    public void AddPrice( float price, int type ) {
        FuelPrice f = new FuelPrice( price, type );
        prices.add(0,f);
        
        if( prices.size() > 50 )
            prices.remove( prices.size()-1);
    }
    public Object[] GetPriceArray() {
        return prices.toArray();
    }
    
    public FuelPrice GetCurStationLastPriceOfType( int type )
    {
        int i = 0;
        while( i < prices.size() )
        {
            if( prices.get(i).getType() == type )
                return prices.get(i);
            i++;
        }
        return null;
    }

    public String getCNPJ() { return CNPJ; }
    public String getNomeFantasia() { return nomeFantasia; }
    public String getEndereco() { return endereco; }
    public String getBairro() { return bairro; }
    public String getCEP() { return CEP; }
    public String getImagem() { return imagem; }
    public String getBandeira() { return bandeira; }
    public String getRazaoSocial() { return razaoSocial; }
    public BufferedImage getIcon() { return icon; }
    @Override public String toString(){
        return razaoSocial;
    }
    
    private void LoadImage(){
        icon = null;
        if( imagem.length() > 0 ){
            try {
                icon = ImageIO.read(new File( imagem ));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        realIcon = null;
        if( icon != null ){
            Image dimg;
            dimg = icon.getScaledInstance( MainGasStationRegistry.view.getImgW(), MainGasStationRegistry.view.getImgH(), Image.SCALE_SMOOTH );
            realIcon = new ImageIcon(dimg);
        }
    }
    public void SetValues(String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira ){
        this.CNPJ = CNPJ;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.endereco = endereco;
        this.bairro = bairro;
        this.CEP = CEP;
        this.imagem = imagem;
        this.bandeira = bandeira;
        
        LoadImage();
    }
    
    public GasStation(){
        prices = new ArrayList();
    }
    public GasStation( String razaoSocial ) {
        this.razaoSocial = razaoSocial;
        prices = new ArrayList();
    }
    public GasStation(String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira) {
        this.CNPJ = CNPJ;
        this.razaoSocial = razaoSocial;
        this.nomeFantasia = nomeFantasia;
        this.endereco = endereco;
        this.bairro = bairro;
        this.CEP = CEP;
        this.imagem = imagem;
        this.bandeira = bandeira;
        
        LoadImage();
        
        prices = new ArrayList();
    }
}
