package RegistryControlPack;

import MainPack.MainGasStationRegistry;
import RegistryModelPack.FuelPrice;

public class RegistryControl {
    
    private String ValidateFields( boolean editMode, String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira )
    {
        if( !editMode ) if( MainGasStationRegistry.model.getStation( razaoSocial ) != null )
            return "Erro: posto '"+razaoSocial+"' já existe";
        if( razaoSocial.length() < 2 )
            return "Erro: Razão social muito curta";
        
        return null;
    }
    
    public String EditStation( String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira){
        String s = ValidateFields( true, CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira );
        if( s == null ){
            MainGasStationRegistry.model.SetCurrentStationValues( CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira );
            return "Posto Editado";
        }
        else return s;
    }
    public String AddNewStation( String CNPJ, String razaoSocial, String nomeFantasia, String endereco, String bairro, String CEP, String imagem, String bandeira){
        String s = ValidateFields( false, CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira );
        if( s == null ){
            MainGasStationRegistry.model.AddNewStation(CNPJ, razaoSocial, nomeFantasia, endereco, bairro, CEP, imagem, bandeira);
            return "Novo posto criado";
        }
        else return s;
    }
    
    public void FilterStation( String bairro ){
         MainGasStationRegistry.model.FilterStation( bairro );
    }
    public void SetSelectedStation( int i ){
        MainGasStationRegistry.model.SetSelectedStation( i );
    }
    public void DeleteSelectedStation( ){
         MainGasStationRegistry.model.DeleteSelectedStation(  );
    }
    
    public String RegisterFuelPrice( String price1, String price2, int type )
    {
        int p1;  //// inteiros
        int p2;  //// centavos
        
        try{ p1 = Integer.parseInt(price1); }
        catch (NumberFormatException e){ return "Preço inválido"; }
        try{ p2 = Integer.parseInt(price2); }
        catch (NumberFormatException e){ return "Preço inválido"; }
        
        if( p1 < 0 ) return "Preço muito pequeno";
        if( p1 > 9999 ) return "Preço muito alto";
        if( p2 < 0 ) return "Preço inválido";
        if( p2 > 99 ) return "Preço inválido";
        
        ///////////// Passou primeiro teste com sucesso
        
        float cent = p2;
        if( price2.length() != 1 )
            cent /= 100;
        else
            cent /= 10;
        cent += p1;
        
        FuelPrice fp = MainGasStationRegistry.model.GetCurStationLastPriceOfType( type );
        if( fp != null )if( fp.getPrice() == cent )
            return "Preço já registrado";
        
        ///////////// Passou segundo teste com sucesso
        
        MainGasStationRegistry.model.RegisterFuelPrice(cent, type);
        return "Preço registrado";
    }
}
